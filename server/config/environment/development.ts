'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/cat'
  },

  whitelistedIPs: [
    'http://localhost:3000',
  ],

  // Seed database on startup
  seedDB: true

};
