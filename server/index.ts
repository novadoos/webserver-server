'use strict';

// Set default node environment to development
const env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if ('production' === process.env.NODE_ENV)
  require('newrelic');

// Export the application
exports = module.exports = require('./app');
