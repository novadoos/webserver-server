'use strict';

const app = require('../..');
import * as request from 'supertest';

describe('Thing API:', function () {
  let newTodo;

  describe('GET /api/todo', function () {
    let todo;

    beforeEach(function (done) {
      request(app)
        .get('/api/todo')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) return done(err);

          todo = res.body;
          done();
        });
    });

    it('should respond with JSON array', function () {
      global.expect(todo).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/todo', function () {
    beforeEach(function (done) {
      request(app)
        .post('/api/todo')
        .send({
          name: 'New Thing',
          info: 'This is the brand new thing!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newTodo = res.body;
          done();
        });
    });

    it('should respond with the newly created thing', function () {
      global.expect(newTodo.name).to.equal('New Thing');
      global.expect(newTodo.info).to.equal('This is the brand new thing!!!');
    });
  });

  describe('DELETE /api/todo/:id', function () {
    it('should respond with 204 on successful removal', function (done) {
      request(app)
        .delete(`/api/todo/${newTodo._id}`)
        .expect(204)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when thing does not exist', function (done) {
      request(app)
        .delete('/api/todo/2')
        .expect(404)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});
