'use strict';

import * as express from 'express';

const TodoController = require('./todo.controller');
const router = express.Router();

router.get('/', TodoController.index);
router.post('/', TodoController.create);
router.delete('/:id', TodoController.destroy);

module.exports = router;
