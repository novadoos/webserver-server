'use strict';

export const proxyquire = require('proxyquire').noPreserveCache();
const todoCtrlStub = {
  index: 'todoCtrl.index',
  create: 'todoCtrl.create',
  destroy: 'todoCtrl.destroy'
};
const routerStub = {
  get: global.sinon.spy(),
  post: global.sinon.spy(),
  delete: global.sinon.spy()
};

// require the index with our stubbed out modules
const todoIndex = proxyquire('./index', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './todo.controller': todoCtrlStub
});

describe('Thing API Router:', function () {
  it('should return an express router instance', function () {
    global.expect(todoIndex).to.equal(routerStub);
  });

  describe('GET /api/todo', function () {
    it('should route to todo.controller.index', function () {
      global.expect(routerStub.get
        .withArgs('/', 'todoCtrl.index')
      ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/todo', function () {
    it('should route to thing.controller.create', function () {
      global.expect(routerStub.post
        .withArgs('/', 'todoCtrl.create')
      ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/todo/:id', function () {
    it('should route to thing.controller.destroy', function () {
      global.expect(routerStub.delete
        .withArgs('/:id', 'todoCtrl.destroy')
      ).to.have.been.calledOnce;
    });
  });
});
