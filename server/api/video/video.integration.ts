'use strict';

const app = require('../..');
import * as request from 'supertest';

describe('Thing API:', function () {
  let newVideo;

  describe('GET /api/videos', function () {
    let video;

    beforeEach(function (done) {
      request(app)
        .get('/api/videos')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) return done(err);

          video = res.body;
          done();
        });
    });

    it('should respond with JSON array', function () {
      global.expect(video).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/videos', function () {
    beforeEach(function (done) {
      request(app)
        .post('/api/videos')
        .send({
          name: 'New Thing',
          info: 'This is the brand new thing!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newVideo = res.body;
          done();
        });
    });

    it('should respond with the newly created thing', function () {
      global.expect(newVideo.name).to.equal('New Thing');
      global.expect(newVideo.info).to.equal('This is the brand new thing!!!');
    });
  });

  describe('DELETE /api/videos/:id', function () {
    it('should respond with 204 on successful removal', function (done) {
      request(app)
        .delete(`/api/videos/${newVideo._id}`)
        .expect(204)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when thing does not exist', function (done) {
      request(app)
        .delete('/api/videos/2')
        .expect(404)
        .end(err => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});
