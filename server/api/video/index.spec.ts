'use strict';

export const proxyquire = require('proxyquire').noPreserveCache();
const videoCtrlStub = {
  index: 'videoCtrl.index',
  create: 'videoCtrl.create',
  destroy: 'videoCtrl.destroy'
};
const videoRouterStub = {
  get: global.sinon.spy(),
  post: global.sinon.spy(),
  delete: global.sinon.spy()
};

// Require the index with our stubbed out modules
const videoIndex = proxyquire('./index', {
  express: {
    Router() {
      return videoRouterStub;
    }
  },
  './video.controller': videoCtrlStub
});

describe('Thing API Router:', function () {
  it('should return an express router instance', function () {
    global.expect(videoIndex).to.equal(videoRouterStub);
  });

  describe('GET /api/videos', function () {
    it('should route to video.controller.index', function () {
      global.expect(videoRouterStub.get
        .withArgs('/', 'videoCtrl.index')
      ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/videos', function () {
    it('should route to thing.controller.create', function () {
      global.expect(videoRouterStub.post
        .withArgs('/', 'videoCtrl.create')
      ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/videos/:id', function () {
    it('should route to thing.controller.destroy', function () {
      global.expect(videoRouterStub.delete
        .withArgs('/:id', 'videoCtrl.destroy')
      ).to.have.been.calledOnce;
    });
  });
});
