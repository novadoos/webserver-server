'use strict';

import * as express from 'express';

const VideoController = require('./video.controller');
const router = express.Router();

router.get('/', VideoController.index);
router.post('/', VideoController.create);
router.delete('/:id', VideoController.destroy);

module.exports = router;
