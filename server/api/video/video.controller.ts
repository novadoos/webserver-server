import * as express from 'express';
import * as Promise from 'promise';

import {Video} from 'models';

class VideoController {
  static index(req: express.Request, res: express.Response): void {
    let _todo = req.body;

    Video.find({}, (error, videos) => {
      console.log(videos.length);
      res.status(200).json(videos);
    });
  }

  static create(req: express.Request, res: express.Response): void {
    let _todo = req.body;

    console.log('createTodo');
    res.status(201).json({ _id: '1', name: 'New Thing', info: 'This is the brand new thing!!!' });
  }

  static destroy(req: express.Request, res: express.Response): void {
    let _id = parseFloat(req.params.id);

    console.log('deleteTodo');

    if (_id === 1) {
      res.status(204).json([]);
    } else {
      res.status(404).json([]);
    }
  }
}

module.exports = VideoController;
