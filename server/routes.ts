/**
 * Main application routes
 */

'use strict';

const errors = require('./components/errors');
import * as path from 'path';

export default function (app) {
  // Insert routes below
  app.use('/api/todo', require('./api/todo/'));
  app.use('/api/videos', require('./api/video/'));

  // app.use('/auth', require('./auth').default);

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
    .get(errors[404]);
}
