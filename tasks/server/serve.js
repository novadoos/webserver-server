import gulp from 'gulp';
let spawn = require('child_process').spawn;
let node;

gulp.task('serve', function() {
  if (node) node.kill();

  node = spawn('node', ['index.js'], { stdio: 'inherit' })
  node.on('close', function(code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });
});

// gulp.task('default', function() {
//   gulp.run('server')

//   gulp.watch(['./index.js', './lib/**/*.js'], function() {
//     gulp.run('server')
//   })
// });
