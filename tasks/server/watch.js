import gulp from 'gulp';
import tsc from 'gulp-typescript';

const TS_CONFIG = './tsconfig.json';
const tsconfigSrc = tsc.createProject(TS_CONFIG);

gulp.task('watch', ['server.compile_tsc'], function() {
  gulp.watch(tsconfigSrc.config.filesGlob, ['server.compile_tsc']);
});
