import gulp from 'gulp';
import tsc from 'gulp-typescript';

const TS_CONFIG = './tsconfig.json';

let tsconfigSrc = tsc.createProject(TS_CONFIG);

//Use this if you want to speed it up
// let tsconfigSrc = tsc.createProject(TS_CONFIG, { isolatedModules: true });

gulp.task('server.compile_tsc', () => {
  tsconfigSrc.src()
    .pipe(tsconfigSrc())
    .js
    .pipe(gulp.dest('build/'));

  return gulp.run('serve');
});
